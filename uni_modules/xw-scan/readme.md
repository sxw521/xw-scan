# xw-scan

## 功能及测试

- 基于uniapp开发，使用在PDA上面,对PDA扫描头部的监听,可以在PDA扫码后获取到对应的数据
- 测试设备是ZEBRA (Android 10),打包app，测试正常
- 该插件仅限于PDA扫描模式为广播模式

## 使用说明

- 插件导入在需要的页面直接使用，下面使用模板
	
```
<template>
	<view class="content">
		<xw-scan></xw-scan>
	</view>
</template>
<script>
	export default {
	onUnload() {
		   // 移除监听事件      
		   uni.$off('xwscan')
		},
		onShow() {
			let that = this
			uni.$off('xwscan') // 每次进来先 移除全局自定义事件监听器
			uni.$on('xwscan', (res)=> {
				console.log('扫码结果：', res.code);
				this.input = res.code
			})
		},
	}
</script>
```